//
//  CSAccountLoginViewController.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-06-30.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSAccountLoginViewController.h"
#import "CSAccount.h"
#import <WCAlertView/WCAlertView.h>




///--------------------------------------------------
/// Private Interface
///--------------------------------------------------
#pragma mark - Private Interface

@interface CSAccountLoginViewController ()

// Configuration
- (void)_configureDeleteAccountButton;
- (void)_configureTextFields;

// IBActions
- (IBAction)_deleteAccountButtonTapped:(id)sender;

// Operations
- (void)_delete;
- (void)_save;

@end





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSAccountLoginViewController
{
	__weak IBOutlet UITextField *_usernameTextField;
	__weak IBOutlet UITextField *_passwordTextField;
	__weak IBOutlet UIButton *_deleteAccountButton;
}





///--------------------------------------------------
/// View Lifecycle
///--------------------------------------------------
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self _configureDeleteAccountButton];
	[self _configureTextFields];
}

- (void)viewDidUnload
{
	_usernameTextField = nil;
	_passwordTextField = nil;
	_deleteAccountButton = nil;
	[super viewDidUnload];
}





///--------------------------------------------------
/// Configuration
///--------------------------------------------------
#pragma mark - Configuration

- (void)_configureDeleteAccountButton
{
	if (nil == _account)
	{
		[_deleteAccountButton setHidden:YES];
	}
}

- (void)_configureTextFields
{
	if (nil != _account)
	{
		[_usernameTextField setText:[_account username]];
		[_passwordTextField setText:[_account password]];
	}
}





///--------------------------------------------------
/// IBActions
///--------------------------------------------------
#pragma mark - IBActions

- (void)_deleteAccountButtonTapped:(id)sender
{
	NSString *title = NSLocalizedString(@"Deleting Account", nil);
	NSString *message = NSLocalizedString(@"Are you sure you want to delete this account?", nil);
	NSString *cancelButtonTitle = NSLocalizedString(@"No", nil);
	NSString *yesButtonTitle = NSLocalizedString(@"Yes", nil);
	
	[WCAlertView showAlertWithTitle:title
							message:message
				 customizationBlock:^(WCAlertView *alertView) {
					 [alertView setStyle:WCAlertViewStyleBlack];
				 }
					completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
						switch (buttonIndex) {
							case 1:
								[self _delete];
								break;
								
							default:
								break;
						}
					}
				  cancelButtonTitle:cancelButtonTitle
				  otherButtonTitles:yesButtonTitle, nil];
}





///--------------------------------------------------
/// Operations
///--------------------------------------------------
#pragma mark - Operations

- (void)_delete
{
	[[CSDataSource defaultDataSource] deleteAccount:_account];
	[[self navigationController] popViewControllerAnimated:YES];
}

- (void)_save
{
	NSCharacterSet *whitespaceAndNewlineCharacterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
	NSString *username = [[_usernameTextField text] stringByTrimmingCharactersInSet:whitespaceAndNewlineCharacterSet];
	NSString *password = [[_passwordTextField text] stringByTrimmingCharactersInSet:whitespaceAndNewlineCharacterSet];
	
	if ([username length] > 0 && [password length] > 0)
	{
		if (nil != _account)
		{
			[_account setUsername:username];
			[_account setPassword:password];
			[[self navigationController] popViewControllerAnimated:YES];
		}
		else
		{
			[[CSDataSource defaultDataSource] newAccountWithUserName:username password:password];
			[[self navigationController] dismissViewControllerAnimated:YES completion:nil];
		}
	}
}





///--------------------------------------------------
/// UITextField Delegate
///--------------------------------------------------
#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if (textField == _usernameTextField)
	{
		[_passwordTextField becomeFirstResponder];
	}
	else if (textField == _passwordTextField)
	{
		[self _save];
	}
	
	return YES;
}


@end
