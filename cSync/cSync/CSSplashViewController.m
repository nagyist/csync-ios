//
//  CSSplashViewController.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-08.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSSplashViewController.h"
#import "CSStoryboardSegue.h"





///--------------------------------------------------
/// Private Interface
///--------------------------------------------------
#pragma mark - Private Interface

@interface CSSplashViewController ()

@end





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSSplashViewController





///--------------------------------------------------
/// View Lifecycel
///--------------------------------------------------
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self performSelector:@selector(_performSegue) withObject:nil afterDelay:1.0f];
}

- (void)_performSegue
{
	[self performSegueWithIdentifier:CSSegueIdShowSlideMenuRoot sender:self];
}


@end
