//
//  CSGoogleContactsService.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-01.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSGoogleContactsService.h"
#import <GDataContacts.h>





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSGoogleContactsService
{
    GDataServiceGoogleContact *_service;
    GDataServiceTicket *_groupFetchTicket;
}





///--------------------------------------------------
/// Initialization
///--------------------------------------------------
#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (nil != self)
    {
        _service = [[GDataServiceGoogleContact alloc] init];
        [_service setShouldCacheResponseData:YES];
        [_service setServiceShouldFollowNextLinks:YES];
    }
    return self;
}

- (id)initWithUsername:(NSString *)username password:(NSString *)password
{
    self = [self init];
    if (nil != self)
    {
        _username = username;
        _password = password;
        [_service setUserCredentialsWithUsername:_username password:_password];
    }
    return self;
}





///--------------------------------------------------
/// Groups
///--------------------------------------------------
#pragma mark - Groups

- (void)fetchContactsForEntryContactGroup:(GDataEntryContactGroup *)entryContactGroup completionHandler:(void (^)(GDataServiceTicket *, GDataFeedBase *, NSError *))completionHandler
{
	NSURL *feedURL = [GDataServiceGoogleContact contactFeedURLForUserID:kGDataServiceDefaultUser];
	GDataQueryContact *query = [GDataQueryContact contactQueryWithFeedURL:feedURL];
	[query setGroupIdentifier:[entryContactGroup identifier]];
	_groupFetchTicket = [_service fetchFeedWithQuery:query completionHandler:completionHandler];
}

- (void)fetchGroupsWithCompletionHandler:(void (^)(GDataServiceTicket *, GDataFeedBase *, NSError *))completionHandler
{
    NSURL *feedURL = [GDataServiceGoogleContact groupFeedURLForUserID:kGDataServiceDefaultUser];
    GDataQueryContact *query = [GDataQueryContact contactQueryWithFeedURL:feedURL];
	[query setShouldShowDeleted:NO];
	[query setMaxResults:2000];
    
    _groupFetchTicket = [_service fetchFeedWithQuery:query completionHandler:completionHandler];
}


@end
