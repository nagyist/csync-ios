//
//  CSAccountTypeViewController.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-06-30.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import <UIKit/UIKit.h>





///--------------------------------------------------
/// Interface
///--------------------------------------------------
#pragma mark - Interface

@interface CSAccountTypeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@end
