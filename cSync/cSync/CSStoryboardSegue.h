//
//  CSStoryboardSegue.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-08.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import <UIKit/UIKit.h>





///--------------------------------------------------
/// Interface
///--------------------------------------------------
#pragma mark - Interface

@interface CSStoryboardSegue : UIStoryboardSegue





///--------------------------------------------------
/// Properties
///--------------------------------------------------
#pragma mark - Properties

@property (nonatomic, assign) UIViewAnimationOptions animationOptions;
@property (nonatomic, assign) NSTimeInterval animationDuration;
@property (nonatomic, copy) void (^animationCompletion)(BOOL finished);


@end
