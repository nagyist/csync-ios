//
//  CSContactsViewController.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-06.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSContactsViewController.h"
#import "CSContactViewController.h"
#import <GDataContacts.h>





///--------------------------------------------------
/// Private Interface
///--------------------------------------------------
#pragma mark - Private Interface

@interface CSContactsViewController ()

// Data
- (void)_sortData;

@end





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSContactsViewController
{
	__weak IBOutlet UITableView *_tableView;
	NSArray *_sortedFeedContact;
	GDataEntryContact *_selectedEntryContact;
}





///--------------------------------------------------
/// View Lifecycle
///--------------------------------------------------
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self _sortData];
}

- (void)viewDidUnload {
	_tableView = nil;
	[super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	NSString *segueId = [segue identifier];
	if ([segueId isEqualToString:CSSegueIdShowContact])
	{
		CSContactViewController *contactViewController = (CSContactViewController *)[segue destinationViewController];
		[contactViewController setContact:_selectedEntryContact];
	}
}





///--------------------------------------------------
/// Data
///--------------------------------------------------
#pragma mark - Data

- (void)_sortData
{
	NSSortDescriptor *sortyByName = [[NSSortDescriptor alloc] initWithKey:@"name"
																ascending:YES
															   comparator:^NSComparisonResult(id obj1, id obj2) {
																   GDataName *name1 = (GDataName *)obj1;
																   GDataName *name2 = (GDataName *)obj2;
																   
																   NSComparisonResult result;
																   
																   NSString *familyName1 = [[name1 familyName] stringValue];
																   NSString *familyName2 = [[name2 familyName] stringValue];
																   result = [familyName1 caseInsensitiveCompare:familyName2];
																   
																   if (result == NSOrderedSame)
																   {
																	   NSString *givenName1 = [[name1 givenName] stringValue];
																	   NSString *givenName2 = [[name2 givenName] stringValue];
																	   result = [givenName1 caseInsensitiveCompare:givenName2];
																   }
																   return result;
															   }];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortyByName, nil];
	_sortedFeedContact = [[_feedContact entries] sortedArrayUsingDescriptors:sortDescriptors];
}





///--------------------------------------------------
/// UITableView Data Source
///--------------------------------------------------
#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = [_sortedFeedContact count];
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *cellIdentifier = [[NSString alloc] initWithFormat:@"%@%@", NSStringFromClass([self class]), @"Cell"];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	GDataEntryContact *entryContact = [_sortedFeedContact objectAtIndex:[indexPath row]];
	NSString *name = [[NSString alloc] initWithFormat:@"%@, %@", entryContact.name.familyName.stringValue, entryContact.name.givenName.stringValue];
	[[cell textLabel] setText:name];
	
	
	return cell;
}





///--------------------------------------------------
/// UITableView Delegate
///--------------------------------------------------
#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	_selectedEntryContact = [_sortedFeedContact objectAtIndex:[indexPath row]];
	[self performSegueWithIdentifier:CSSegueIdShowContact sender:self];
}



@end
