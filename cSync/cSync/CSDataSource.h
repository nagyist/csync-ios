//
//  CSDataSource.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-06-30.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CSAccount;

extern NSString * const CSEntityName;





///--------------------------------------------------
/// Interface
///--------------------------------------------------
#pragma mark - Interface

@interface CSDataSource : NSObject





///--------------------------------------------------
/// Properties
///--------------------------------------------------
#pragma mark - Properties

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;





///--------------------------------------------------
/// Singleton
///--------------------------------------------------
#pragma mark - Singleton

+ (id)defaultDataSource;

///--------------------------------------------------
/// Core Data Utils
///--------------------------------------------------
#pragma mark - Core Data Utils

- (void)saveContext;

///--------------------------------------------------
/// General Utils
///--------------------------------------------------
#pragma mark - General Utils

- (NSURL *)applicationDocumentsDirectory;





///--------------------------------------------------
/// Accounts
///--------------------------------------------------
#pragma mark - Accounts

- (CSAccount *)newAccountWithUserName:(NSString *)username password:(NSString *)password;
- (void)deleteAccount:(CSAccount *)account;


@end
