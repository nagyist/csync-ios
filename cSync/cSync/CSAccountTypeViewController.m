//
//  CSAccountTypeViewController.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-06-30.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSAccountTypeViewController.h"
#import "CSAccountTypeCell.h"





///--------------------------------------------------
/// Private Interface
///--------------------------------------------------
#pragma mark - Private Interface

@interface CSAccountTypeViewController ()

// Configurations
- (void)_configureToolbarItems;
- (void)_configureTitle;

// IBActions
- (IBAction)_cancelButtonTapped:(id)sender;

@end





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSAccountTypeViewController
{
	__weak IBOutlet UITableView *_tableView;
}





///--------------------------------------------------
/// View Lifecycle
///--------------------------------------------------
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self _configureTitle];
	[self _configureToolbarItems];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	_tableView = nil;
	[super viewDidUnload];
}





///--------------------------------------------------
/// Configurations
///--------------------------------------------------
#pragma mark - Configurations

- (void)_configureToolbarItems
{
	UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(_cancelButtonTapped:)];
	
	NSArray *toolbarItems = [[NSArray alloc] initWithObjects:flexibleSpace, cancelButton, nil];
	[self setToolbarItems:toolbarItems];
}

- (void)_configureTitle
{
	NSString *title = NSLocalizedString(@"Select Account Type", nil);
	[self setTitle:title];
}





///--------------------------------------------------
/// IBActions
///--------------------------------------------------
#pragma mark - IBActions

- (void)_cancelButtonTapped:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}





///--------------------------------------------------
/// UITableView Data Source
///--------------------------------------------------
#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *cellIdentifier = [[NSString alloc] initWithFormat:@"%@%@", NSStringFromClass([self class]), @"Cell"];
	CSAccountTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	UIImage *logoImage = [UIImage imageNamed:@"CS-AccountType-Logo-Gmail.png"];
	[[cell logoImageView] setImage:logoImage];
	return cell;
}





///--------------------------------------------------
/// UITableView Delegate
///--------------------------------------------------
#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self performSegueWithIdentifier:CSSegueIdShowAccountLogin sender:self];
}


@end
