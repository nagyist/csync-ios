//
//  CSAccountTypeCell.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-08.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSAccountTypeCell.h"





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSAccountTypeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

