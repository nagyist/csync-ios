//
//  CSSlideMenuRootViewController.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-20.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "SASlideMenuRootViewController.h"

@interface CSSlideMenuRootViewController : SASlideMenuRootViewController

@end
