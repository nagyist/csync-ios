//
//  CSViewController.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-06-30.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSAccountsViewController.h"

#import "CSAccount.h"
#import "CSAccountLoginViewController.h"
#import "CSContactGroupsViewController.h"
#import "CSGoogleContactsService.h"

#import <MBProgressHUD/MBProgressHUD.h>
#import <WCAlertView/WCAlertView.h>





///--------------------------------------------------
/// Private Interface
///--------------------------------------------------
#pragma mark - Private Interface

@interface CSAccountsViewController ()

// Configurations
- (void)_configureFetchedResultsController;
- (void)_configureToolbarItems;
- (void)_configureTitle;

// IBActions
- (IBAction)_addAccountButtonTapped:(id)sender;

// Refreshing the content
- (void)_refreshContent;

@end





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSAccountsViewController
{
	__weak IBOutlet UITableView *_tableView;
	
	// Toolbar Items
	UIBarButtonItem *_addAccountButton;
	
	//
	NSFetchedResultsController *_fetchedResultsController;
    
    //
    GDataFeedContactGroup *_feedContactGroup;
    CSAccount *_selectedAccount;
}





///--------------------------------------------------
/// View Lifecycle
///--------------------------------------------------
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self _configureTitle];
	[self _configureToolbarItems];
	[self _configureFetchedResultsController];
	
	[self _refreshContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
	_tableView = nil;
	[super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *segueId = [segue identifier];
    if ([segueId isEqualToString:CSSegueIdShowContactGroups])
    {
        CSContactGroupsViewController *contactGroupViewController = (CSContactGroupsViewController *)[segue destinationViewController];
        [contactGroupViewController setAccount:_selectedAccount];
        [contactGroupViewController setFeedContactGroup:_feedContactGroup];
    }
	else if ([segueId isEqualToString:CSSegueIdShowAccountLogin])
	{
		CSAccountLoginViewController *accountLoginViewController = (CSAccountLoginViewController *)[segue destinationViewController];
		[accountLoginViewController setAccount:_selectedAccount];
	}
}





///--------------------------------------------------
/// Configurations
///--------------------------------------------------
#pragma mark - Configurations

- (void)_configureFetchedResultsController
{
	CSDataSource *dataSource = [CSDataSource defaultDataSource];
	NSManagedObjectContext *context = [dataSource managedObjectContext];
	
	NSSortDescriptor *sortByUsername = [[NSSortDescriptor alloc] initWithKey:@"username" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortByUsername, nil];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:CSEntityName inManagedObjectContext:context];
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:entity];
	[fetchRequest setSortDescriptors:sortDescriptors];
	
	NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
																							   managedObjectContext:context
																								 sectionNameKeyPath:nil
																										  cacheName:NSStringFromClass([self class])];
	_fetchedResultsController = fetchedResultsController;
	[_fetchedResultsController setDelegate:self];
}

- (void)_configureToolbarItems
{
	UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	
	_addAccountButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(_addAccountButtonTapped:)];
	
	NSArray *toolbarItems = [[NSArray alloc] initWithObjects:flexibleSpace, _addAccountButton, nil];
	[self setToolbarItems:toolbarItems];
}

- (void)_configureTitle
{
	NSString *title = NSLocalizedString(@"Accounts", nil);
	[self setTitle:title];
}





///--------------------------------------------------
/// IBActions
///--------------------------------------------------
#pragma mark - IBActions

- (void)_addAccountButtonTapped:(id)sender
{
	[self performSegueWithIdentifier:CSSegueIdShowAccountTypeSelection sender:self];
}





///--------------------------------------------------
/// Refreshing the content
///--------------------------------------------------
#pragma mark - Refreshing the content

- (void)_refreshContent
{
	NSError *fetchError = nil;
	[_fetchedResultsController performFetch:&fetchError];
	
	[_tableView reloadData];
}





///--------------------------------------------------
/// NSFetchedResultsController Delegate
///--------------------------------------------------
#pragma mark - NSFetchedResultsController Delegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	[self _refreshContent];
}





///--------------------------------------------------
/// UITableView Data Source
///--------------------------------------------------
#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	id<NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
	return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *cellIdentifier = [[NSString alloc] initWithFormat:@"%@%@", NSStringFromClass([self class]), @"Cell"];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	CSAccount *account = [_fetchedResultsController objectAtIndexPath:indexPath];
	
	[[cell textLabel] setText:[account username]];
	return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		NSString *title = NSLocalizedString(@"Deleting Account", nil);
		NSString *message = NSLocalizedString(@"Are you sure you want to delete this account?", nil);
		NSString *cancelButtonTitle = NSLocalizedString(@"No", nil);
		NSString *yesButtonTitle = NSLocalizedString(@"Yes", nil);
		
		[WCAlertView showAlertWithTitle:title
								message:message
					 customizationBlock:^(WCAlertView *alertView) {
						 [alertView setStyle:WCAlertViewStyleBlack];
					 }
						completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
							switch (buttonIndex) {
								case 1:
								{
									CSAccount *account = [_fetchedResultsController objectAtIndexPath:indexPath];
									CSDataSource *dataSource = [CSDataSource defaultDataSource];
									[dataSource deleteAccount:account];
									break;
								}
									
								default:
									break;
							}
						}
					  cancelButtonTitle:cancelButtonTitle
					  otherButtonTitles:yesButtonTitle, nil];
	}
}





///--------------------------------------------------
/// UITableView Delegate
///--------------------------------------------------
#pragma mark - UiTableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    _selectedAccount = [_fetchedResultsController objectAtIndexPath:indexPath];
	
	MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
	[progressHUD setMode:MBProgressHUDModeText];
	[progressHUD setDetailsLabelText:NSLocalizedString(@"Please wait while we attempt to retrieve information.", nil)];
	
	CSGoogleContactsService *contactService = [[CSGoogleContactsService alloc] initWithUsername:[_selectedAccount username] password:[_selectedAccount  password]];
	[contactService fetchGroupsWithCompletionHandler:^(GDataServiceTicket *ticket, GDataFeedBase *feed, NSError *error) {
		if (nil != error)
		{
			[self _dismissAllHUDs];
			NSString *errorMessage = [[NSString alloc] initWithFormat:@"Code: %d %@", [error code], [[error userInfo] objectForKey:@"error"]];
			MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
			[progressHUD setMode:MBProgressHUDModeText];
			[progressHUD setDetailsLabelText:errorMessage];
			[self performSelector:@selector(_dismissAllHUDs) withObject:nil afterDelay:3.0f];
		}
		else
		{
			[self _dismissAllHUDs];
			_feedContactGroup = (GDataFeedContactGroup *)feed;
			[self performSegueWithIdentifier:CSSegueIdShowContactGroups sender:self];
		}
	}];
    
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	_selectedAccount = [_fetchedResultsController objectAtIndexPath:indexPath];
	[self performSegueWithIdentifier:CSSegueIdShowAccountLogin sender:self];
}

- (void)_dismissAllHUDs
{
	[MBProgressHUD hideAllHUDsForView:[self view] animated:YES];
}


@end
