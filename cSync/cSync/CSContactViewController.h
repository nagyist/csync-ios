//
//  CSContactViewController.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-06.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GDataEntryContact;





///--------------------------------------------------
/// Interface
///--------------------------------------------------
#pragma mark - Interface

@interface CSContactViewController : UIViewController





///--------------------------------------------------
/// Properties
///--------------------------------------------------
#pragma mark - Properties

@property (nonatomic, strong) GDataEntryContact *contact;


@end
