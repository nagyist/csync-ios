//
//  CSSlideMenuViewController.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-20.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSSlideMenuViewController.h"





///--------------------------------------------------
/// Private Interface
///--------------------------------------------------
#pragma mark - Private Interface

@interface CSSlideMenuViewController ()

@end





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSSlideMenuViewController





///--------------------------------------------------
/// View Lifecycle
///--------------------------------------------------
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (NSIndexPath *)selectedIndexPath
{
	return [NSIndexPath indexPathForRow:0 inSection:0];
}

- (NSString *)segueIdForIndexPath:(NSIndexPath *)indexPath
{
	return CSSegueIdShowAccounts;
}

- (void)configureMenuButton:(UIButton *)menuButton
{
	UIImage *menuButtonImage = [UIImage imageNamed:@"CS-SlideMenu-Icon.png"];
	CGRect menuButtonFrame = CGRectMake(0, 0, menuButtonImage.size.width, menuButtonImage.size.height);
	[menuButton setFrame:menuButtonFrame];
	[menuButton setImage:menuButtonImage forState:UIControlStateNormal];
}


@end
