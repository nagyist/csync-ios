//
//  CSAccountTypeCell.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-08.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import <UIKit/UIKit.h>





///--------------------------------------------------
/// Interface
///--------------------------------------------------
#pragma mark - Interface

@interface CSAccountTypeCell : UITableViewCell





///--------------------------------------------------
/// Properties
///--------------------------------------------------
#pragma mark - Properties

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;


@end
