//
//  CSContactsViewController.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-06.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GDataFeedContact;




///--------------------------------------------------
/// Interface
///--------------------------------------------------
#pragma mark - Interface

@interface CSContactsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>





///--------------------------------------------------
/// Properties
///--------------------------------------------------
#pragma mark - Properties

@property (nonatomic, strong) GDataFeedContact *feedContact;


@end
