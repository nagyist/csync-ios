//
//  CSSlideMenuRootViewController.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-20.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSSlideMenuRootViewController.h"

@interface CSSlideMenuRootViewController ()

@end

@implementation CSSlideMenuRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
