//
//  CSConstants.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-06-30.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

///--------------------------------------------------
/// Segues
///--------------------------------------------------
#pragma mark - Segues

extern NSString * const CSSegueIdShowSlideMenuRoot;
extern NSString * const CSSegueIdShowAccounts;
extern NSString * const CSSegueIdShowAccountTypeSelection;
extern NSString * const CSSegueIdShowAccountLogin;
extern NSString * const CSSegueIdShowContactGroups;
extern NSString * const CSSegueIdShowContacts;
extern NSString * const CSSegueIdShowContact;
