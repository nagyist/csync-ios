//
//  CSAccount.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-06-30.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSAccount.h"


@implementation CSAccount

@dynamic username;
@dynamic password;
@dynamic typeId;
@dynamic typeName;

@end
