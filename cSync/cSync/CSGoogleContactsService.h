//
//  CSGoogleContactsService.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-01.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSBaseService.h"

@class GDataEntryContactGroup;
@class GDataFeedBase;
@class GDataServiceTicket;





///--------------------------------------------------
/// Interface
///--------------------------------------------------
#pragma mark - Interface

@interface CSGoogleContactsService : CSBaseService





///--------------------------------------------------
/// Properties
///--------------------------------------------------
#pragma mark - Properties

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;





///--------------------------------------------------
/// Initialization
///--------------------------------------------------
#pragma mark - Initialization

- (id)initWithUsername:(NSString *)username password:(NSString *)password;

///--------------------------------------------------
/// Groups
///--------------------------------------------------
#pragma mark - Groups

- (void)fetchContactsForEntryContactGroup:(GDataEntryContactGroup *)entryContactGroup completionHandler:(void(^)(GDataServiceTicket *ticket, GDataFeedBase *feed, NSError *error))completionHandler;
- (void)fetchGroupsWithCompletionHandler:(void(^)(GDataServiceTicket *ticket, GDataFeedBase *feed, NSError *error))completionHandler;


@end
