//
//  CSConstants.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-06-30.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

///--------------------------------------------------
/// Segues
///--------------------------------------------------
#pragma mark - Segues

NSString * const CSSegueIdShowSlideMenuRoot = @"CSSegueIdShowSlideMenuRoot";
NSString * const CSSegueIdShowAccounts = @"CSSegueIdShowAccounts";
NSString * const CSSegueIdShowAccountTypeSelection = @"CSSegueIdShowAccountTypeSelection";
NSString * const CSSegueIdShowAccountLogin = @"CSSegueIdShowAccountLogin";
NSString * const CSSegueIdShowContactGroups = @"CSSegueIdShowContactGroups";
NSString * const CSSegueIdShowContacts = @"CSSegueIdShowContacts";
NSString * const CSSegueIdShowContact = @"CSSegueIdShowContact";
