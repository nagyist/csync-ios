//
//  CSSlideMenuViewController.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-20.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import <SASlideMenu/SASlideMenuViewController.h>
#import <SASlideMenu/SASlideMenuDataSource.h>
#import <SASlideMenu/SASlideMenuDelegate.h>

@interface CSSlideMenuViewController : SASlideMenuViewController <SASlideMenuDataSource, SASlideMenuDelegate>

@end
