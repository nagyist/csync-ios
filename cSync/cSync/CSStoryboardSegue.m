//
//  CSStoryboardSegue.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-08.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSStoryboardSegue.h"





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSStoryboardSegue





///--------------------------------------------------
/// Initialization
///--------------------------------------------------
#pragma mark - Initialization

- (id)initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination
{
	self = [super initWithIdentifier:identifier source:source destination:destination];
	if (nil != self)
	{
		UIWindow *window = [[source view] window];
		if (nil == window)
		{
			window = [[[UIApplication sharedApplication] windows] lastObject];
		}
		_animationDuration = 1.0f;
		_animationOptions = UIViewAnimationOptionTransitionCurlUp;
		_animationCompletion = ^(BOOL finished) {
			[window setRootViewController:destination];
		};
	}
	return self;
}





///--------------------------------------------------
/// Performing the Segue
///--------------------------------------------------
#pragma mark - Performing the Segue

- (void)perform
{
	UIViewController *sourceViewController = [self sourceViewController];
	UIViewController *destinationViewController = [self destinationViewController];
	
	[UIView transitionFromView:[sourceViewController view]
						toView:[destinationViewController view]
					  duration:[self animationDuration]
					   options:[self animationOptions]
					completion:[self animationCompletion]];
}


@end
