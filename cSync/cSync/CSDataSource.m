//
//  CSDataSource.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-06-30.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSDataSource.h"
#import "CSAccount.h"

NSString * const CSEntityName = @"CSAccount";





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSDataSource

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;





///--------------------------------------------------
/// Singleton
///--------------------------------------------------
#pragma mark - Singleton

+ (id)defaultDataSource
{
	static CSDataSource *_defaultDataSource = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_defaultDataSource = [[CSDataSource alloc] init];
	});
	return _defaultDataSource;
}





///--------------------------------------------------
/// Core Data Utils
///--------------------------------------------------
#pragma mark - Core Data Utils

- (void)saveContext
{
	NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			// Replace this implementation with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"cSync" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"cSync.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}





///--------------------------------------------------
/// General Utils
///--------------------------------------------------
#pragma mark - General Utils

- (NSURL *)applicationDocumentsDirectory
{
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}





///--------------------------------------------------
/// Accounts
///--------------------------------------------------
#pragma mark - Accounts

- (CSAccount *)newAccountWithUserName:(NSString *)username password:(NSString *)password
{
	NSManagedObjectContext *context = [self managedObjectContext];
	NSEntityDescription *entity = [NSEntityDescription entityForName:CSEntityName inManagedObjectContext:context];
	CSAccount *account = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
	[account setTypeName:@"gmail"];
	[account setPassword:password];
	[account setUsername:username];
	
	[self saveContext];
	
	return account;
}

- (void)deleteAccount:(CSAccount *)account
{
	NSManagedObjectContext *context = [self managedObjectContext];
	[context deleteObject:account];
	[self saveContext];
}


@end
