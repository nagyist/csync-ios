//
//  CSContactViewController.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-06.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSContactViewController.h"

#import <GDataContacts.h>





///--------------------------------------------------
/// Private Interface
///--------------------------------------------------
#pragma mark - Private Interface

@interface CSContactViewController ()

@end





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSContactViewController
{
	__weak IBOutlet UIImageView *_photoImageView;
	__weak IBOutlet UITextField *_firstNameTextField;
	__weak IBOutlet UITextField *_lastNameTextField;
	__weak IBOutlet UITextField *_emaiTextField;
}





///--------------------------------------------------
/// View Lifecycle
///--------------------------------------------------
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[_lastNameTextField setText:[[[_contact name] familyName] stringValue]];
	[_firstNameTextField setText:[[[_contact name] givenName] stringValue]];
	
	GDataEmail *email = [[_contact emailAddresses] objectAtIndex:0];
	[_emaiTextField setText:[email address]];
	
	NSURL *imageURL = [[NSURL alloc] initWithString:_contact.photoLink.href];
	NSData *imageData = [[NSData alloc] initWithContentsOfURL:imageURL];
	UIImage *photoImage = [UIImage imageWithData:imageData];
	[_photoImageView setImage:photoImage];
	
}

- (void)viewDidUnload {
	_photoImageView = nil;
	_firstNameTextField = nil;
	_lastNameTextField = nil;
	_emaiTextField = nil;
	_photoImageView = nil;
	[super viewDidUnload];
}


@end
