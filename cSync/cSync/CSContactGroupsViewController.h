//
//  CSContactGroupsViewController.h
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-01.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GDataFeedContactGroup;





///--------------------------------------------------
/// Interface
///--------------------------------------------------
#pragma mark - Interface

@interface CSContactGroupsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>





///--------------------------------------------------
/// Properties
///--------------------------------------------------
#pragma mark - Properties

@property (nonatomic, strong) CSAccount *account;
@property (nonatomic, strong) GDataFeedContactGroup *feedContactGroup;


@end
