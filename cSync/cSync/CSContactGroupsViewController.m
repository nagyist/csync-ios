//
//  CSContactGroupsViewController.m
//  cSync
//
//  Created by Benoit Sarrazin on 2013-07-01.
//  Copyright (c) 2013 Kings Of Spades. All rights reserved.
//

#import "CSContactGroupsViewController.h"
#import "CSAccount.h"
#import "CSGoogleContactsService.h"
#import "CSContactsViewController.h"
#import <GDataContacts.h>
#import <MBProgressHUD/MBProgressHUD.h>





///--------------------------------------------------
/// Private Interface
///--------------------------------------------------
#pragma mark - Private Interface

@interface CSContactGroupsViewController ()

@end





///--------------------------------------------------
/// Implementation
///--------------------------------------------------
#pragma mark - Implementation

@implementation CSContactGroupsViewController
{
    __weak IBOutlet UITableView *_tableView;
	GDataFeedContact *_feedContact;
}





///--------------------------------------------------
/// View Lifecycle
///--------------------------------------------------
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *title = [_account username];
    [self setTitle:title];
}

- (void)viewDidUnload
{
    _tableView = nil;
    [super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	NSString *segueId = [segue identifier];
	if ([segueId isEqualToString:CSSegueIdShowContacts])
	{
		CSContactsViewController *contactsViewController = (CSContactsViewController *)[segue destinationViewController];
		[contactsViewController setFeedContact:_feedContact];
	}
}





///--------------------------------------------------
/// UITableView Data Source
///--------------------------------------------------
#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = [[_feedContactGroup entries] count];
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = [[NSString alloc] initWithFormat:@"%@%@", NSStringFromClass([self class]), @"Cell"];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    GDataEntryContactGroup *entryContactGroup = [[_feedContactGroup entries] objectAtIndex:[indexPath row]];
	NSString *title = [[entryContactGroup systemGroup] stringValue];
	if (nil == title)
	{
		title = [[entryContactGroup title] stringValue];
	}
    [[cell textLabel] setText:title];
    return cell;
}





///--------------------------------------------------
/// UITableView Delegate
///--------------------------------------------------
#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	GDataEntryContactGroup *entryContactGroup = [[_feedContactGroup entries] objectAtIndex:[indexPath row]];
	
	MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
	[progressHUD setMode:MBProgressHUDModeText];
	[progressHUD setDetailsLabelText:NSLocalizedString(@"Please wait while we attempt to retrieve information.", nil)];
	
	CSGoogleContactsService *contactService = [[CSGoogleContactsService alloc] initWithUsername:[_account username] password:[_account  password]];
	[contactService fetchContactsForEntryContactGroup:entryContactGroup
									completionHandler:^(GDataServiceTicket *ticket, GDataFeedBase *feed, NSError *error) {
										_feedContact = (GDataFeedContact *)feed;
										[MBProgressHUD hideAllHUDsForView:[self view] animated:YES];
										[self performSegueWithIdentifier:CSSegueIdShowContacts sender:self];
									}];
}


@end
